import random
import os
import sys
import numpy as np
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.pool import NullPool

TOTAL_ROUNDS = 10

NUMBER_OF_OPTIONS_IMBALANCE_FORECAST = 5
NUMBER_OF_OPTIONS_ASSET_FORECAST = 5

INTRADAY_PRECISION_MODIFIER = 5
IMBALANCE_PRECISION_MODIFIER = 5
ASSET_PRECISION_MODIFIER = 50

IMBALANCE_PROFIT_MODIFIER = 5
ASSET_PROFIT_MODIFIER = 1


def randomize_options(number_of_options):
    return random.sample(range(1, number_of_options + 1), number_of_options)


class PostgresLoader:
    """Base class to connect with AWS postgres instance"""

    def __init__(self):
        host = os.environ['POSTGRES_HOST']
        username = os.environ['POSTGRES_USER']
        password = os.environ['POSTGRES_PASSWORD']
        self.engine = create_engine("postgresql://" + username + ":" + password + "@" + host, poolclass=NullPool)
        self._create_base()
        self._get_game_table()

    def _create_base(self):
        self.base = automap_base()
        self.base.prepare(self.engine, reflect=True, schema='townhall')
        return self

    def _get_game_table(self):
        self.table = self.base.classes.profits2
        return self

    def load_row(self, rows):
        for row in rows:
            table = self.table(**row)
            session = Session(self.engine)
            session.merge(table)
            session.commit()
            session.close_all()


def load_to_postgres(row):
    loader = PostgresLoader()
    loader.load_row(row)


def generate_intraday_outturn():
    return random.normalvariate(mu=35, sigma=20)


def generate_imbalance_outturn(intraday_outturn):
    return intraday_outturn + random.normalvariate(mu=0, sigma=20)


def generate_asset_outturn():
    return random.normalvariate(mu=1000, sigma=500)


def generate_imbalance_forecast(imbalance_outturn, precision):
    return imbalance_outturn + random.normalvariate(mu=0, sigma=precision * IMBALANCE_PRECISION_MODIFIER)


def generate_intraday_forecast(intraday_outturn, precision):
    return intraday_outturn + random.normalvariate(mu=0, sigma=precision * INTRADAY_PRECISION_MODIFIER)


def generate_asset_forecast(asset_outturn, precision):
    return asset_outturn + random.normalvariate(mu=0, sigma=precision * ASSET_PRECISION_MODIFIER)


def generate_scores():
    intraday_outturn = generate_intraday_outturn()
    asset_outturn = generate_asset_outturn()
    imbalance_outturn = generate_imbalance_outturn(intraday_outturn)

    imbalance_forecast_precisions = randomize_options(NUMBER_OF_OPTIONS_IMBALANCE_FORECAST)
    asset_forecast_precisions = randomize_options(NUMBER_OF_OPTIONS_ASSET_FORECAST)
    imbalance_forecasts = [generate_imbalance_forecast(imbalance_outturn, i) for i in imbalance_forecast_precisions]
    intraday_forecasts = [generate_intraday_forecast(intraday_outturn, i) for i in imbalance_forecast_precisions]
    asset_forecasts = [generate_asset_forecast(asset_outturn, i) for i in asset_forecast_precisions]

    out = {
        'intraday_outturn': intraday_outturn,
        'asset_outturn': asset_outturn,
        'imbalance_outturn': imbalance_outturn,
    }
    for forecast_id in range(len(imbalance_forecast_precisions)):
        out['imbalance_price_forecast_' + str(forecast_id + 1)] = imbalance_forecasts[forecast_id]
        out['intraday_forecast_' + str(forecast_id + 1)] = intraday_forecasts[forecast_id]
        out['asset_forecast_' + str(forecast_id + 1)] = asset_forecasts[forecast_id]
        

    return out


def calculate_profit(imbalance_price_forecast, imbalance_outturn, intraday_forecast, intraday_outturn, asset_forecast, asset_outturn):
    error_imbalance = np.abs(imbalance_price_forecast - imbalance_outturn)
    error_asset = np.abs(asset_forecast - asset_outturn)
    return 2000 - error_asset * ASSET_PROFIT_MODIFIER - error_imbalance * IMBALANCE_PROFIT_MODIFIER


def get_all_profits(round):
    scores = generate_scores()
    imbalance_outturn = scores['imbalance_outturn']
    asset_outturn = scores['asset_outturn']
    intraday_outturn = scores['intraday_outturn']
    profits = []
    for forecast_id_imbalance in range(NUMBER_OF_OPTIONS_IMBALANCE_FORECAST): 
        for forecast_id_asset in range(NUMBER_OF_OPTIONS_ASSET_FORECAST):
            scores_tmp = dict()
            scores_tmp['round'] = round
            scores_tmp['forecast_id_imbalance'] = str(forecast_id_imbalance)
            scores_tmp['forecast_id_asset'] = str(forecast_id_asset)
            imbalance_price_forecast = scores['imbalance_price_forecast_' + str(forecast_id_imbalance + 1)]
            intraday_forecast = scores['intraday_forecast_' + str(forecast_id_imbalance + 1)]
            asset_forecast = scores['imbalance_price_forecast_' + str(forecast_id_asset + 1)]
            scores_tmp['profit'] = calculate_profit(imbalance_price_forecast, imbalance_outturn,
                                                    intraday_forecast, intraday_outturn,
                                                    asset_forecast, asset_outturn)
            profits.append(scores_tmp)
    return profits


def run():
    for round_ in range(TOTAL_ROUNDS):
        all_profits = get_all_profits(round_ + 1)
        load_to_postgres(all_profits)
