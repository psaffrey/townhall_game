#!/usr/bin/env python3

import csv
import urllib.request
import os
import psycopg2

DOC_LOCATION="https://docs.google.com/spreadsheets/d/1HVWoPUru5v-Mx7nnU5kewfYCnz9brcRl6RcMCCVqTyU/export?format=csv"

INSERT_SQL = """
    INSERT INTO submissions("user", "asset_forecast", "imbalance_forecast")
    VALUES(%s, %s, %s);
  """

DELETE_SQL = """
  DELETE FROM submissions
"""

ASSET_FORECAST_MAPPING = {
  "Origami": 1,
  "Inhouse": 2,
  "Crystal ball": 3,
  "Tarot cards": 4,
  "Fish entrails": 5
}



def clear_table(conn):
  cur = conn.cursor()
  cur.execute(DELETE_SQL)
  conn.commit()

def read_and_upload(conn):
  cur = conn.cursor()
  with urllib.request.urlopen(DOC_LOCATION) as htmlstream:
    output = htmlstream.read().decode('utf-8').split('\r\n')
    reader = csv.DictReader(output)
    for row in reader:
      print(f"insert row: {row}")
      asset_forecast_id = ASSET_FORECAST_MAPPING[row["Pick your asset forecast"]]
      imbalance_forecast_id = ASSET_FORECAST_MAPPING[row["Pick your imbalence price forecast"]]
      cur.execute(INSERT_SQL, (row["Participant Name"], asset_forecast_id, imbalance_forecast_id))
  conn.commit()
      

def connect_to_database():
  host = os.environ['POSTGRES_HOST']
  username = os.environ['POSTGRES_USER']
  password = os.environ['POSTGRES_PASSWORD']
  dbname = "postgres"
  schema = "townhall"

  conn = psycopg2.connect(host=host, database=dbname, user=username, password=password, options=f'-c search_path={schema}')

  return conn

def insert(row, conn):
  pass

if __name__ == "__main__":
  conn = connect_to_database()
  clear_table(conn)
  read_and_upload(conn)